<?php

class User{

    // database connection and table name
    private $conn;
    private $table_name = "user";

    // object properties
    public $email;
    public $roles;
    public $password;
    public $firstname;
    public $lastname;
    public $phone;
    public $deleted_at = null;
    public $payment_method = "CB";
    public $address;
    public $zipcode;
    public $city;
    public $civility;
    public $token = null;
    public $username;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read products
    function read(){

        // select all query
        $query = "SELECT
                u.email, u.roles,u.password,
                u.firstname,u.lastname,u.phone,
                u.deleted_at,u.payment_method,
                u.address,u.zipcode,u.city,u.civility,
                u.token,u.username
            FROM
                " . $this->table_name . " u ";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }

    // create product
    function create(){

        // query to insert record
        $query = "INSERT INTO
                " . $this->table_name . "
            SET
                email=:email, roles=:roles,
                password=:password,firstname=:firstname,
                lastname=:lastname,
                phone=:phone,address=:address,
                city=:city,civility=:civility,token=:token,
                username=:username,deleted_at=:deleted_at,
                payment_method=:payment_method,zipcode=:zipcode
                ";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->password=htmlspecialchars(strip_tags($this->password));
        $this->firstname=htmlspecialchars(strip_tags($this->firstname));
        $this->lastname=htmlspecialchars(strip_tags($this->lastname));
        $this->phone=htmlspecialchars(strip_tags($this->phone));
        $this->address=htmlspecialchars(strip_tags($this->address));
        $this->payment_method=htmlspecialchars(strip_tags($this->payment_method));
        $this->zipcode=htmlspecialchars(strip_tags($this->zipcode));
        $this->city=htmlspecialchars(strip_tags($this->city));
        $this->civility=htmlspecialchars(strip_tags($this->civility));
        $this->token=htmlspecialchars(strip_tags($this->token));
        $this->username=htmlspecialchars(strip_tags($this->username));

        // bind values
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":password", $this->password);
        $stmt->bindParam(":firstname", $this->firstname);
        $stmt->bindParam(":lastname", $this->lastname);
        $stmt->bindParam(":phone", $this->phone);
        $stmt->bindParam(":deleted_at", $this->deleted_at);
        $stmt->bindParam(":payment_method", $this->payment_method);
        $stmt->bindParam(":address", $this->address);
        $stmt->bindParam(":zipcode", $this->zipcode);
        $stmt->bindParam(":city", $this->city);
        $stmt->bindParam(":civility", $this->civility);
        $stmt->bindParam(":token", $this->token);
        $stmt->bindParam(":username", $this->username);
        $data = json_encode($this->roles);
        $stmt->bindParam(":roles", $data);


        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;

    }
}